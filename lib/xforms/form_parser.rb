require 'nokogiri'

module XForms
  class FormParser
    XFORMS_NS = 'http://www.w3.org/2002/xforms'

    def initialize(input)
      @reader = Nokogiri::XML::Reader(input)
      @form = Form.new
      @bindings = {}
    end

    def parse
      while read
        if is_xforms_element?
          parse_xforms_element
        end
      end
      @form.recalculate
      @form
    end

    private

    def parse_xforms_element
      case @reader.local_name
      when 'model' then parse_model
      when 'input' then parse_input
      when 'select1' then parse_select1
      end
    end

    def parse_model
      read_descendants do
        if is_xforms_element? 'instance'
          @form.model_instance = Nokogiri::XML(@reader.inner_xml)
        elsif is_xforms_element? 'bind'
          parse_bind
        elsif is_element? 'itext'
          parse_itext
        end
      end
    end

    def parse_bind
      @bindings[@reader.attribute('id')] = {
        :nodeset => @reader.attribute('nodeset'),
        :readonly => @reader.attribute('readonly'),
        :constraint => @reader.attribute('constraint'),
        :constraint_message => @reader.attribute("#{jr_prefix}:constraintMsg"),
        :relevant => @reader.attribute('relevant'),
        :type => parse_type(@reader.attribute('type'))
      }

      if @reader.attribute('calculate')
        @form.calculates << { :binding => @reader.attribute('nodeset'), :value => XPathRef.new(@form, @reader.attribute('calculate')) }
      end
    end

    def parse_type type
      return type if type.nil?
      type.split(':').last
    end

    def jr_prefix
      ns = @form.namespaces.key('http://openrosa.org/javarosa')
      ns.split(':').last if ns
    end

    def parse_input
      parse_control XForms::Input
    end

    def parse_select1
      parse_control XForms::Select1 do |select1|
        if is_xforms_element? 'item'
          select1.items << parse_item
        end
      end
    end

    def parse_item
      item = XForms::Item.new @form
      read_descendants do
        if is_xforms_element? 'label'
          item.label = parse_label
        elsif is_xforms_element? 'value'
          item.value = read_inner_text
        end
      end
      item
    end

    def parse_control type
      @form.controls << control = type.new(@form)

      if ref = @reader.attribute('ref')
        control.binding = ref
      elsif bind = @reader.attribute('bind')
        binding = @bindings[bind]
        control.binding = binding[:nodeset]
        control.readonly = XPathRef.new(@form, binding[:readonly]) if binding[:readonly]
        control.constraint = binding[:constraint]
        control.constraint_message = binding[:constraint_message]
        control.relevant = XPathRef.new(@form, binding[:relevant]) if binding[:relevant]
        control.type = binding[:type]
      end

      read_descendants do
        if is_xforms_element? 'label'
          control.label = parse_label
        elsif block_given?
          yield control
        end
      end
    end

    def parse_label
      if @reader.attribute('ref')
        XPathRef.new @form, @reader.attribute('ref')
      else
        read_inner_text
      end
    end

    def parse_itext
      @form.itext = Hash.new { |h,k| h[k] = Hash.new { |h,k| h[k] = {} } }
      read_descendants do
        if is_element? 'translation'
          lang = @reader.attribute('lang')
          read_descendants do
            if is_element? 'text'
              id = @reader.attribute('id')
              read_descendants do
                @form.itext[lang][id] = read_inner_text if is_element? 'value'
              end
            end
          end
        end
      end
    end

    def read
      node = @reader.read
      @form.namespaces.merge! node.namespaces if node
      node
    end

    def read_inner_text
      read_descendants do
        if @reader.node_type == Nokogiri::XML::Reader::TYPE_TEXT
          return @reader.value
        end
      end
    end

    def read_descendants
      unless @reader.self_closing?
        current_depth = @reader.depth
        while read && @reader.depth > current_depth
          yield
        end
      end
    end

    def is_element?(name, ns = nil)
      @reader.node_type == Nokogiri::XML::Reader::TYPE_ELEMENT && @reader.namespace_uri == ns && (name.nil? || @reader.local_name == name)
    end

    def is_xforms_element?(name = nil)
      is_element? name, XFORMS_NS
    end
  end
end