require 'spec_helper'
require 'xforms'
require 'stringio'

describe "Form" do

  let(:xform1_path) { File.expand_path '../data/xform1.xml', __FILE__ }
  let(:form) { XForms::Form.parse_file xform1_path }
  let(:input) { form.controls[0] }
  let(:select1) { form.controls[1] }
  let(:model) { form.model_instance }

  it "parse model instance" do
    model.root.name.should == 'data'
  end

  it "parse from string" do
    form = XForms::Form.parse File.read(xform1_path)
    form.model_instance.root.name.should == 'data'
  end

  it "parse from IO" do
    form = XForms::Form.parse StringIO.new(File.read(xform1_path))
    form.model_instance.root.name.should == 'data'
  end

  it "parses input" do
    input.should be_a(XForms::Input)
    input.label.should == 'Foo'
  end

  it "parses select1" do
    select1.should be_a(XForms::Select1)
    select1.label.should == 'Bar'
    select1.items[0].label.should == 'Item1'
    select1.items[0].value.should == '1'
    select1.items[1].label.should == 'Item2'
    select1.items[1].value.should == '2'
  end

  it "set label correctly after empty element" do
    form.controls[3].label.should == 'Baz'
  end

  it "set input value" do
    input.value = 'something'
    model.xpath('x:data/x:foo').inner_text.should == 'something'
  end

  it "get input value" do
    input.value.should == '123'
  end

  it "set select value" do
    select1.value = '1'
    model.xpath('x:data/x:bar').inner_text.should == '1'
  end

  it "uses binding for third control" do
    form.controls[2].value = 'something_else'
    model.xpath('x:data/x:baz').inner_text.should == 'something_else'
  end

  it "parse readonly" do
    form.controls[4].readonly.should == true
  end

  it "readonly is false by default" do
    form.controls[2].readonly.should == false
  end

  it "reject invalid value" do
    form.controls[2].value = '55'
    form.controls[2].should_not be_valid
  end

  it "accept valid value" do
    form.controls[2].value = '5'
    form.controls[2].should be_valid
  end

  it "field without constraint is always valid" do
    form.controls[0].should be_valid
  end

  it "field with constraint message" do
    form.controls[2].constraint_message.should == 'Error Message'
  end

  it "irrelevant field" do
    form.controls[5].should_not be_relevant
  end

  it "relevant field" do
    form.controls[0].value = '321'
    form.controls[5].should be_relevant
  end

  it "is relevant if no condition is specified" do
    form.controls[0].should be_relevant
  end

  it "input type int" do
    form.controls[6].type.should == 'int'
  end

  it "get input value with int type" do
    form.controls[6].value.should == 123
  end

  it "get input value with int type when data is empty" do
    model.xpath('x:data/x:foo').first.children = ''
    form.controls[6].value.should == 0
  end

  it "set input value with int type" do
    form.controls[6].value = '01'
    model.xpath('x:data/x:foo').inner_text.should == '1'
  end

  it "set int value to integer input" do
    form.controls[6].value = 1
    model.xpath('x:data/x:foo').inner_text.should == '1'
  end

  it "execute2 calc1 binding" do
    model.xpath('x:data/x:calc1').inner_text.should == '246.0'
  end

  it "executes calc2 binding" do
    model.xpath('x:data/x:calc2').inner_text.should == '123'
  end

  it "executes calc3 binding with now function" do
    model.xpath('x:data/x:calc3').inner_text.should == Time.now.utc.iso8601
  end

end